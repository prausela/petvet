## DISCLAIMER
This repository is a mirror of another. The original repository is in the following link: https://github.com/shipupi/IngeSoft

# IngeSoft
TPetPack's web-based solution for PetVet's Delivery Service

## Installation instructions

### Requirements

- python3, pip

### Pip packages

```
		pip install django
		pip django-credit-cards
		pip install parameterized
		pip install Pillow
```

### Installation steps

#### Clone this repository 

```
	git clone https://github.com/shipupi/IngeSoft.git
```

#### Enter the server folder

```
		cd IngeSoft/3.\ Development/PetVet/
```

#### Run the server

```
		python3 manage.py runserver --insecure
```

## Completed

### Check Inside "1. Analisis y Relevamiento" Files Outside Folder UCD
* Vision
* SRS
* UCP
* UCD
* Use Case Specification
